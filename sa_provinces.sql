--
-- Table structure for table `sa_provinces`
--

CREATE TABLE `sa_provinces` (
  `id` int(11) NOT NULL,
  `nameAr` varchar(64) NOT NULL,
  `nameEn` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='SA provinces';

--
-- Dumping data for table `sa_provinces`
--

INSERT INTO `sa_provinces` (`id`, `nameAr`, `nameEn`) VALUES
(1, ' الحدود الشمالية', 'Northern Borders'),
(2, ' الجوف', 'Al Jawf'),
(3, ' تبوك', 'Tabuk '),
(4, ' حائل', 'Ha\'il'),
(5, ' القصيم', 'Al-Qassim'),
(6, ' الرياض', 'Riyadh'),
(7, ' المدينة المنورة', 'Medina'),
(8, ' عسير', '\'Asir'),
(9, ' الباحة', 'Al Bahah\r\n'),
(10, 'جيزان', 'Jizan '),
(11, ' مكة المكرمة', 'Mecca'),
(12, ' نجران', 'Najran'),
(13, 'المنطقة الشرقية', 'Eastern Province');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sa_provinces`
--
ALTER TABLE `sa_provinces`
  ADD UNIQUE KEY `id` (`id`);
COMMIT;